# Echo the contents of a file
f = open('moby.txt', 'rU')
num_lines = 0
dict = {}
for line in f:
    # line = line.replace('\n', ' ')
    words = line.split(' ')
    if len(words) == 0:
        print('skip empty line')
        continue

    # Add words into the dictionary
    for word in words:
        if word in dict:
            dict[word] += 1
        else:
            dict[word] = 1
    num_lines += 1

# Store the dic in a list of tuples (word, count) in decending order of count
list = []
for word, count in dict.items():
    list.append((word, count))
list = sorted(list, key = lambda t: t[1], reverse = True)
print(list[:100])

print('the text has {} lines'.format(num_lines))