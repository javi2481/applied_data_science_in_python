
# coding: utf-8

# ---
# 
# _You are currently looking at **version 1.5** of this notebook. To download notebooks and datafiles, as well as get help on Jupyter notebooks in the Coursera platform, visit the [Jupyter Notebook FAQ](https://www.coursera.org/learn/python-data-analysis/resources/0dhYG) course resource._
# 
# ---

# # Assignment 3 - More Pandas
# This assignment requires more individual learning then the last one did - you are encouraged to check out the [pandas documentation](http://pandas.pydata.org/pandas-docs/stable/) to find functions or methods you might not have used yet, or ask questions on [Stack Overflow](http://stackoverflow.com/) and tag them as pandas and python related. And of course, the discussion forums are open for interaction with your peers and the course staff.

# ### Question 1 (20%)
# 
# 
# Load the energy data from the file `Energy Indicators.xls`, which is a list of indicators of [energy supply and renewable electricity production](Energy%20Indicators.xls) from the [United Nations](http://unstats.un.org/unsd/environment/excel_file_tables/2013/Energy%20Indicators.xls) for the year 2013, and should be put into a DataFrame with the variable name of **energy**.
# 
# Keep in mind that this is an Excel file, and not a comma separated values file. Also, make sure to exclude the footer and header information from the datafile. The first two columns are unneccessary, so you should get rid of them, and you should change the column labels so that the columns are:
# 
# `['Country', 'Energy Supply', 'Energy Supply per Capita', '% Renewable']`
# 
# Convert `Energy Supply` to gigajoules (there are 1,000,000 gigajoules in a petajoule). For all countries which have missing data (e.g. data with "...") make sure this is reflected as `np.NaN` values.
# 
# Rename the following list of countries (for use in later questions):
# 
# ```"Republic of Korea": "South Korea",
# "United States of America": "United States",
# "United Kingdom of Great Britain and Northern Ireland": "United Kingdom",
# "China, Hong Kong Special Administrative Region": "Hong Kong"```
# 
# There are also several countries with numbers and/or parenthesis in their name. Be sure to remove these, 
# 
# e.g. 
# 
# `'Bolivia (Plurinational State of)'` should be `'Bolivia'`, 
# 
# `'Switzerland17'` should be `'Switzerland'`.
# 
# <br>
# 
# Next, load the GDP data from the file `world_bank.csv`, which is a csv containing countries' GDP from 1960 to 2015 from [World Bank](http://data.worldbank.org/indicator/NY.GDP.MKTP.CD). Call this DataFrame **GDP**. 
# 
# Make sure to skip the header, and rename the following list of countries:
# 
# ```"Korea, Rep.": "South Korea", 
# "Iran, Islamic Rep.": "Iran",
# "Hong Kong SAR, China": "Hong Kong"```
# 
# <br>
# 
# Finally, load the [Sciamgo Journal and Country Rank data for Energy Engineering and Power Technology](http://www.scimagojr.com/countryrank.php?category=2102) from the file `scimagojr-3.xlsx`, which ranks countries based on their journal contributions in the aforementioned area. Call this DataFrame **ScimEn**.
# 
# Join the three datasets: GDP, Energy, and ScimEn into a new dataset (using the intersection of country names). Use only the last 10 years (2006-2015) of GDP data and only the top 15 countries by Scimagojr 'Rank' (Rank 1 through 15). 
# 
# The index of this DataFrame should be the name of the country, and the columns should be ['Rank', 'Documents', 'Citable documents', 'Citations', 'Self-citations',
#        'Citations per document', 'H index', 'Energy Supply',
#        'Energy Supply per Capita', '% Renewable', '2006', '2007', '2008',
#        '2009', '2010', '2011', '2012', '2013', '2014', '2015'].
# 
# *This function should return a DataFrame with 20 columns and 15 entries.*

# In[32]:

import numpy as np
import re
import pandas as pd
    
## Function preparing dataframe 'energy'
def get_energy():    
    xls = pd.ExcelFile('Energy Indicators.xls')
    # print(type(xls))
    energy = pd.read_excel(xls, 'Energy', skiprows=16, skip_footer=38)
    # print(type(energy))

    ## Remove the first two columns
    del energy["Unnamed: 0"]
    del energy["Unnamed: 1"]

    ## Rename columns
    energy = energy.rename(columns={"Unnamed: 2": "Country", "Energy Supply per capita": "Energy Supply per Capita", "Renewable Electricity Production": "% Renewable"})

    ## Some hard-coded contry name changes
    energy["Country"] = energy["Country"].str.replace("Republic of Korea", "South Korea")
    energy["Country"] = energy["Country"].str.replace("United States of America", "United States")
    energy["Country"] = energy["Country"].str.replace("United Kingdom of Great Britain and Northern Ireland", "United Kingdom")
    energy["Country"] = energy["Country"].str.replace("China, Hong Kong Special Administrative Region", "Hong Kong")

    ## Remove (...) part from country names
    energy["Country"] = energy["Country"].str.replace(r" \([a-zA-Z]+( [a-zA-Z]+)*\)", "")

    ## Use compiled regex somehow does not work!
    # regex_pat = re.compile(' \([a-zA-Z]+( [a-zA-Z]+)*\)$')
    # energy["Country"] = energy['Country'].str.replace(regex_pat, '')

    ## Why the index is 0-based? and the display is 1-based?
    # print(energy.index)

    # Remove digits attached to contry names
    energy["Country"] = energy["Country"].str.replace(r"\d+$", "")

    # Replace ... by NaN
    energy = energy.replace("...", np.nan)
    #print(energy)
        
    ## Drop the first row
    # energy = energy.drop(energy.index[0])
    energy = energy.iloc[1:]
    
    ## Change the unit of energy supply to jogajoules from petajoles by multiplying the value by 1000000
    energy["Energy Supply"] = energy["Energy Supply"].astype("float64")
    energy["Energy Supply"] = 1000000.0*energy["Energy Supply"]    
    # print(energy["Energy Supply"].dtype) # is float16
    
    energy["Energy Supply per Capita"] = energy["Energy Supply per Capita"].astype("float64")
    
    # Save the dataframe into a csv file -- Doesn't work!
    # energy.to_csv("Energy.csv")
    return energy

## Function preparing dataframe 'GDP'
def get_GDP():
    GDP = pd.read_csv('world_bank.csv', skiprows = 4)

    ## Some hard-coded contry name changes
    GDP["Country Name"] = GDP["Country Name"].str.replace("Korea, Rep.", "South Korea")
    GDP["Country Name"] = GDP["Country Name"].str.replace("Iran, Islamic Rep.", "Iran")
    GDP["Country Name"] = GDP["Country Name"].str.replace("Hong Kong SAR, China", "Hong Kong")

    ## Remove columns from "1960" to "2005"
    GDP = GDP[["Country Name", "Country Code", "Indicator Name", "Indicator Code", "2006","2007","2008","2009","2010","2011","2012","2013","2014", "2015"]]

    ## Drop "Country Code", "Indicator Name", "Indicator Code", since we don't need them
    GDP = GDP.drop(["Country Code", "Indicator Name", "Indicator Code"], axis = 1)

    ## Rename "Country Name" to "Country"
    GDP = GDP.rename(columns = {"Country Name": "Country"})    
    return GDP

   
## Finally let's prepare dataframe 'GDP'
def get_ScimEn():  
    xlsx = pd.ExcelFile('scimagojr-3.xlsx')
    # print(type(xlsx))
    ScimEn = pd.read_excel(xlsx, 'Sheet1')
    return ScimEn

def answer_one():  
    energy = get_energy()
    GDP = get_GDP()
    ScimEn = get_ScimEn()
    
    ScimEn_Top15 = ScimEn[ScimEn["Rank"] < 16]
    # print(ScimEn_Top15)

    ## Inner join ScimEn, energy, and GDP on "country" 
    df = pd.merge(left = ScimEn_Top15, right = energy, how = "inner", on = "Country")
    df = pd.merge(left = df, right = GDP, how = "inner", on = "Country")

    ## Set Country as the index of the dataframe
    df = df.set_index("Country")    
    return df

answer_one()


# ### Question 2 (6.6%)
# The previous question joined three datasets then reduced this to just the top 15 entries. When you joined the datasets, but before you reduced this to the top 15 items, how many entries did you lose?
# 
# *This function should return a single number.*

# In[2]:

get_ipython().run_cell_magic('HTML', '', '<svg width="800" height="300">\n  <circle cx="150" cy="180" r="80" fill-opacity="0.2" stroke="black" stroke-width="2" fill="blue" />\n  <circle cx="200" cy="100" r="80" fill-opacity="0.2" stroke="black" stroke-width="2" fill="red" />\n  <circle cx="100" cy="100" r="80" fill-opacity="0.2" stroke="black" stroke-width="2" fill="green" />\n  <line x1="150" y1="125" x2="300" y2="150" stroke="black" stroke-width="2" fill="black" stroke-dasharray="5,3"/>\n  <text  x="300" y="165" font-family="Verdana" font-size="35">Everything but this!</text>\n</svg>')


# In[33]:

def answer_two():
    energy = get_energy()
    GDP = get_GDP()
    ScimEn = get_ScimEn()
    
    # Union A, B, C - Intersection A, B, C
    union = pd.merge(pd.merge(energy, GDP, on='Country', how='outer'), ScimEn, on='Country', how='outer')
    intersect = pd.merge(pd.merge(energy, GDP, on='Country'), ScimEn, on='Country')
    return len(union)-len(intersect)

answer_two()


# <br>
# 
# Answer the following questions in the context of only the top 15 countries by Scimagojr Rank (aka the DataFrame returned by `answer_one()`)

# ### Question 3 (6.6%)
# What is the average GDP over the last 10 years for each country? (exclude missing values from this calculation.)
# 
# *This function should return a Series named `avgGDP` with 15 countries and their average GDP sorted in descending order.*

# In[34]:

def answer_three():
    A = answer_one()
    
    ## Add a average column    
    A["avg_gdp"] = A[["2006","2007","2008","2009","2010","2011","2012","2013","2014","2015"]].mean(axis = 1);
    avg_gdp = A["avg_gdp"]    
    avg_gdp = avg_gdp.order(ascending = False)    
    return avg_gdp

answer_three()


# ### Question 4 (6.6%)
# By how much had the GDP changed over the 10 year span for the country with the 6th largest average GDP?
# 
# *This function should return a single number.*

# In[35]:

def answer_four():
    ## Get the #6 contry in average gpd
    A = answer_three()
    country_6 = A.index[5]
    
    ## Get the change in gdp over 10 yesrs for the country
    B = answer_one() # Is anything get copied? I guess not
    gdp = B.loc[country_6] # Is anything get copied? I guess not
    # print(type(gdp_6)) # Series
    delta_gdp = gdp["2015"] - gdp["2006"]
    return delta_gdp

answer_four()


# ### Question 5 (6.6%)
# What is the mean `Energy Supply per Capita`?
# 
# *This function should return a single number.*

# In[36]:

def answer_five():
    A = answer_one()
    e_mean = A["Energy Supply per Capita"].mean()
    #print(A["Energy Supply per Capita"].dtype)
    #print(type(float(e_mean)))
    return float(e_mean)

answer_five()


# ### Question 6 (6.6%)
# What country has the maximum % Renewable and what is the percentage?
# 
# *This function should return a tuple with the name of the country and the percentage.*

# In[37]:

def answer_six():
    A = answer_one()
    renewable = A["% Renewable"]
    renewable = renewable.order(ascending = False)
    ret = [];
    ret.append(renewable.index[0])
    ret.append(renewable.iloc[0])
    return tuple(ret)

answer_six()


# ### Question 7 (6.6%)
# Create a new column that is the ratio of Self-Citations to Total Citations. 
# What is the maximum value for this new column, and what country has the highest ratio?
# 
# *This function should return a tuple with the name of the country and the ratio.*

# In[38]:

def answer_seven():
    A = answer_one()
    A["Ratio citations"] = A["Self-citations"]/A["Citations"];
    s = A["Ratio citations"]
    s = s.order(ascending = False)
    # print(s)
    ret = []
    ret.append(s.index[0])
    ret.append(s.iloc[0])
    return tuple(ret)

answer_seven()


# ### Question 8 (6.6%)
# 
# Create a column that estimates the population using Energy Supply and Energy Supply per capita. 
# What is the third most populous country according to this estimate?
# 
# *This function should return a single string value.*

# In[39]:

def answer_eight():
    A = answer_one()
    A["Popualation estimated"] = A["Energy Supply"]/A["Energy Supply per Capita"];
    s = A["Popualation estimated"]
    s = s.order(ascending = False)
    # print (s)
    return s.index[2]

answer_eight()


# ### Question 9 (6.6%)
# Create a column that estimates the number of citable documents per person. 
# What is the correlation between the number of citable documents per capita and the energy supply per capita? Use the `.corr()` method, (Pearson's correlation).
# 
# *This function should return a single number.*
# 
# *(Optional: Use the built-in function `plot9()` to visualize the relationship between Energy Supply per Capita vs. Citable docs per Capita)*

# In[40]:

def answer_nine():
    A = answer_one()
    
    ## Get eastimation of population
    A["Popualation estimated"] = A["Energy Supply"]/A["Energy Supply per Capita"];
    
    ## Add a column for citable documents per capita
    A["Citable docs per Capita"] = A["Citable documents"]/A["Popualation estimated"] 
    
    ## Why the type of A["Citable docs per capita"] is object?
    A["Citable docs per Capita"] = A["Citable docs per Capita"].astype("float64")
    # print(A["Citable docs per capita"])
    
    A["Energy Supply per Capita"]= A["Energy Supply per Capita"].astype("float64")    
    # print(A["Energy Supply per capita"])
    
    ## Compute the correlation
    r = A["Citable docs per Capita"].corr(A["Energy Supply per Capita"])
    
    return r

answer_nine()


# In[11]:

#def plot9():
#    import matplotlib as plt
#    %matplotlib inline
    
#    Top15 = answer_one()
#    Top15['PopEst'] = Top15['Energy Supply'] / Top15['Energy Supply per Capita']
#    Top15['Citable docs per Capita'] = Top15['Citable documents'] / Top15['PopEst']
#    Top15.plot(x='Citable docs per Capita', y='Energy Supply per Capita', kind='scatter', xlim=[0, 0.0006])


# In[12]:

#plot9() # Be sure to comment out plot9() before submitting the assignment!


# ### Question 10 (6.6%)
# Create a new column with a 1 if the country's % Renewable value is at or above the median for all countries in the top 15, and a 0 if the country's % Renewable value is below the median.
# 
# *This function should return a series named `HighRenew` whose index is the country name sorted in ascending order of rank.*

# In[41]:

def answer_ten():
    A  = answer_one()
    
    ## Get the median of % renewable
    m = A["% Renewable"].median()
    
    ## Create a categorical column for % renewable
    A["HighRenew"] = A["% Renewable"] >= m;
    A["HighRenew"] = A["HighRenew"].sort_index()    
    A["HighRenew"] = A["HighRenew"].astype("int")
    
    # print(type(A["HighRenew"]))
        
    return A["HighRenew"]

answer_ten()


# ### Question 11 (6.6%)
# Use the following dictionary to group the Countries by Continent, then create a dateframe that displays the sample size (the number of countries in each continent bin), and the sum, mean, and std deviation for the estimated population of each country.
# 
# ```python
# ContinentDict  = {'China':'Asia', 
#                   'United States':'North America', 
#                   'Japan':'Asia', 
#                   'United Kingdom':'Europe', 
#                   'Russian Federation':'Europe', 
#                   'Canada':'North America', 
#                   'Germany':'Europe', 
#                   'India':'Asia',
#                   'France':'Europe', 
#                   'South Korea':'Asia', 
#                   'Italy':'Europe', 
#                   'Spain':'Europe', 
#                   'Iran':'Asia',
#                   'Australia':'Australia', 
#                   'Brazil':'South America'}
# ```
# 
# *This function should return a DataFrame with index named Continent `['Asia', 'Australia', 'Europe', 'North America', 'South America']` and columns `['size', 'sum', 'mean', 'std']`*

# In[42]:

def answer_eleven():    
    A = answer_one()
    
    countries = ['China', 'United States', 'Japan','United Kingdom','Russian Federation', 'Canada','Germany','India','France','South Korea','Italy','Spain','Iran','Australia', 'Brazil']
    continents = ["Asia", 'North America', "Asia","Europe", "Europe",'North America', "Europe", "Asia","Europe", "Asia", "Europe", "Europe","Asia","Australia", "South America"]
    dict = {"Country":countries, "Continent": continents}
    df2 = pd.DataFrame(dict)
    # print (df2)    
    
    ## Merge df2 with energy on Country
    df3 = pd.merge(left = df2, right = A, left_on = "Country",right_index = True)
    
    ## Add a column with eastimated population
    df3["Population estimated"] = df3["Energy Supply"]/df3["Energy Supply per Capita"].astype("float")    
    df3["Population estimated"] = df3["Population estimated"].astype("float") # Doesn't work if not doing the conversion
    
    ## One hack of aggregations! I forget the semantics not to say the syntax!
    df3 = df3.set_index('Continent').groupby(level=0)["Population estimated"].agg({'sum':np.sum,'mean':np.average,'size':np.size,   'std':np.std})
    df3['size'] = df3['size'].astype("int")
    
    ## Rearrange column position
    df3 = df3[['size','sum','mean','std']]
    return df3

answer_eleven()


# ### Question 12 (6.6%)
# Cut % Renewable into 5 bins. Group Top15 by the Continent, as well as these new % Renewable bins. How many countries are in each of these groups?
# 
# *This function should return a __Series__ with a MultiIndex of `Continent`, then the bins for `% Renewable`. Do not include groups with no countries.*

# In[43]:

def answer_twelve():
    df = answer_one()    
    df = df["% Renewable"]
    df = df.reset_index()    
    dict = {"China":"Asia", 'United States':'North America', 'Japan':"Asia",'United Kingdom':"Europe",
            'Russian Federation':"Europe", 'Canada':'North America','Germany':"Europe",'India':"Asia",
            'France':"Europe",'South Korea':"Asia",'Italy':"Europe",'Spain':"Europe",'Iran':"Asia",
            'Australia':"Australia", 'Brazil':"South America"}
    # print(dict)
    df["Continent"] = df["Country"].map(dict)
    df["Bins"] = pd.cut(df["% Renewable"], 5, labels = ["lowest", "low", "medium", "high", "highest"])            
    
    ## 2-level aggregation
    # df = df.groupby(["Continent", "Bins"])["Country"].agg("count")
    df = df.groupby(["Continent", "Bins"]).agg("count")
    df = df.dropna()
    # print(df)
    return df["Country"]

answer_twelve()


# ### Question 13 (6.6%)
# Convert the Population Estimate series to a string with thousands separator (using commas). Do not round the results.
# 
# e.g. 317615384.61538464 -> 317,615,384.61538464
# 
# *This function should return a Series `PopEst` whose index is the country name and whose values are the population estimate string.*

# In[44]:

def answer_thirteen():
    # pd.options.display.precision = 10
    df = answer_one()
    print(df["Energy Supply"].dtype)
    print(df["Energy Supply per Capita"].dtype)
    df["PopEst"] = df["Energy Supply"]/df["Energy Supply per Capita"]
    print(df["PopEst"].dtype)
    # df = df["PopEst"]
    s = df["PopEst"]
    s = s.order(ascending = False)    
    
    ## Formating 
    for index, item in s.iteritems():
        s[index] = "{:,}".format(s[index])    
    
    return s

answer_thirteen()


# ### Optional
# 
# Use the built in function `plot_optional()` to see an example visualization.

# In[17]:

def plot_optional():
    import matplotlib as plt
    get_ipython().magic('matplotlib inline')
    Top15 = answer_one()
    ax = Top15.plot(x='Rank', y='% Renewable', kind='scatter', 
                    c=['#e41a1c','#377eb8','#e41a1c','#4daf4a','#4daf4a','#377eb8','#4daf4a','#e41a1c',
                       '#4daf4a','#e41a1c','#4daf4a','#4daf4a','#e41a1c','#dede00','#ff7f00'], 
                    xticks=range(1,16), s=6*Top15['2014']/10**10, alpha=.75, figsize=[16,6]);

    for i, txt in enumerate(Top15.index):
        ax.annotate(txt, [Top15['Rank'][i], Top15['% Renewable'][i]], ha='center')

    print("This is an example of a visualization that can be created to help understand the data. This is a bubble chart showing % Renewable vs. Rank. The size of the bubble corresponds to the countries' 2014 GDP, and the color corresponds to the continent.")


# In[18]:

#plot_optional() # Be sure to comment out plot_optional() before submitting the assignment!

